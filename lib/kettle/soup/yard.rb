# frozen_string_literal: true

require_relative "yard/version"

module Kettle
  module Soup
    module Yard
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
